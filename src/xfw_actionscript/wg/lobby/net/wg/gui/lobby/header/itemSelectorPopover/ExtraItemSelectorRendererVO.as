package net.wg.gui.lobby.header.itemSelectorPopover
{
    public class ExtraItemSelectorRendererVO extends ItemSelectorRendererVO
    {

        public var mainLabel:String = "";

        public var infoLabel:String = "";

        public function ExtraItemSelectorRendererVO(param1:Object)
        {
            super(param1);
        }
    }
}
