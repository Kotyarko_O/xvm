package net.wg.gui.bootcamp.battleResult.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class RewardVideoDataVO extends DAAPIDataClass
    {

        public var label:String = "";

        public var image:String = "";

        public function RewardVideoDataVO(param1:Object)
        {
            super(param1);
        }
    }
}
