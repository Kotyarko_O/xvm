package flashx.textLayout.formats
{
    public final class VerticalAlign extends Object
    {

        public static const TOP:String = "top";

        public static const BOTTOM:String = "bottom";

        public static const MIDDLE:String = "middle";

        public static const JUSTIFY:String = "justify";

        public function VerticalAlign()
        {
            super();
        }
    }
}
